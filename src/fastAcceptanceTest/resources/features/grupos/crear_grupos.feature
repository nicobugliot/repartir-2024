# language: es
Característica: Crear Grupo para repartir gastos

  Regla: Los grupos están compuestos por al menos dos miembros

    Escenario: No puedo crear un grupo con un único miembro
      Cuando el usuario intenta crear un grupo indicando un único miembro
      Entonces no debería crear el grupo con un único miembro

  Regla: Los grupos no pueden estar compuestos por 10 miembros o más

    Escenario: No puedo crear un grupo con 10 miembros
      Cuando el usuario intenta crear un grupo indicando 10 miembros
      Entonces no debería crear el grupo

    Escenario: Crea un grupo con 9 miembros
      Cuando el usuario intenta crear un grupo indicando 9 miembros
      Entonces el sistema debería crear el grupo con éxito y tener 9 miembros