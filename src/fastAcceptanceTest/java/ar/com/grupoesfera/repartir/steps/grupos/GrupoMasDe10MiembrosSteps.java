package ar.com.grupoesfera.repartir.steps.grupos;

import ar.com.grupoesfera.repartir.model.Grupo;
import ar.com.grupoesfera.repartir.steps.FastCucumberSteps;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Entonces;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GrupoMasDe10MiembrosSteps extends FastCucumberSteps {

    private Grupo grupo;

    @Cuando("el usuario intenta crear un grupo indicando {int} miembros")
    public void elUsuarioIntentaCrearUnGrupoIndicandoMiembros(int cantidadMiembros) {
        grupo = new Grupo();
        List<String> miembros = new ArrayList<>();
        for (int i = 0; i < cantidadMiembros; i++) {
            miembros.add("Miembro " + (i + 1));
        }
        grupo.setMiembros(miembros);
    }

    @Entonces("no debería crear el grupo")
    public void elSistemaDeberíaDevolverUnErrorIndicandoQueSeAlcanzóElMáximoDeMiembrosParaUnGrupo() {
        assertFalse(grupo.estaFormado());
    }

    @Entonces("el sistema debería crear el grupo con éxito y tener {int} miembros")
    public void elSistemaDeberíaCrearElGrupoConÉxitoYTenerMiembros(int cantidadMiembros) {
        assertTrue(grupo.estaFormado());
        assertEquals(cantidadMiembros, grupo.getMiembros().size());
    }
}
